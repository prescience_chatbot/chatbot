import os
import numpy as np
import pandas as pd
# from selenium import webdriver
import plotly.plotly as pya
#
# driver=webdriver.PhantomJS()
import pickle
import quandl
from datetime import datetime
import plotly.offline as py
import plotly.graph_objs as go
import plotly.figure_factory as ff
py.init_notebook_mode(connected=True)

def get_quandl_data(quandl_id):
    '''Download and cache Quandl dataseries'''
    cache_path = '{}.pkl'.format(quandl_id).replace('/','-')
    try:
        f = open(cache_path, 'rb')
        df = pickle.load(f)
        print('Loaded {} from cache'.format(quandl_id))
    except (OSError, IOError) as e:
        print('Downloading {} from Quandl'.format(quandl_id))
        df = quandl.get(quandl_id, returns="pandas",authtoken='8YEnMD5jgnzdzz4sBixY')
        df.to_pickle(cache_path)
        print('Cached {} at {}'.format(quandl_id, cache_path))
    return df

btc_usd_price_kraken = get_quandl_data('BCHARTS/KRAKENUSD')
btc_usd_price_kraken.head()
btc_trace = go.Scatter(x=btc_usd_price_kraken.index, y=btc_usd_price_kraken['Weighted Price'])
py.plot([btc_trace])
























from flask import Flask, render_template, request
from flask import jsonify
import accessing_database
import pandas as pd
import requests
import requests

import json
app = Flask(__name__,static_url_path="/static")

import datetime
from datetime import timedelta
import re



#############################################################################################################

#first layer start

main_categories = {('price', 'price?', 'amount', 'cost', 'demand', 'estimate', 'fare', 'fee', 'figure', 'output', 'pay',
                    'payment', 'premium', 'rate', 'return', 'score', 'wages', 'sell''price-point', 'priced',
                    'discounted', 'rate', 'difference', 'disparity', 'msrp', 'jump', 'nzd', 'differential', 'deal',
                    'msrp.', 'usd.', 'resale', 'tag', 'value', 'points', 'drop', 'sell', 'decrease', 'rrp',
                    'pricepoint', 'pricing', 'level', 'discount', '$100', 'margin', 'cost', 'jumps', 'costs',
                    '80+', 'cut', 'point', 'prices', 'hike', 'change', 'mark-up', 'pricetag', 'drops', 'way', 'market',
                    'increase', 'premium', 'cap', 'increases','was'): 'price',
                   ('philosophy', 'views', 'criticism', 'think', 'merits', 'argue', 'opionion', 'agree', 'disagreeing',
                    'sentiment', 'opnion', 'preference', 'dissagree', 'obviously', 'points', 'feelings', 'dislike',
                    'opinion', 'opinons', 'objectively', 'counterargument', 'counterpoint', 'opinions', 'feeling',
                    'bias', 'regard', 'arguements', 'thoughts', 'stance', 'standpoints', 'one', 'oppinions', 'honesty',
                    'matter', 'view', 'reasons', 'merit', 'opinion', 'argument', 'arguments', 'viewpoint', 'arguement',
                    'oppinion', 'again', 'reasoning', 'mate', 'point', 'criticisms', 'assessment', 'opinion',
                    'disagree', 'fact', 'opinon', 'statement', 'perspective', 'standpoint', 'preferences', 'disliking',
                    'opinion', 'honesty', 'pessimism', 'vehemence', 'paragraph', 'views', 'way', 'reaction',
                    'disagreeable', 'viewpoint', 'distaste', 'generalisation', 'debate', 'condescension',
                    'wholeheartedly', 'flippancy', 'comment', 'conversation', 'commenter', 'point', 'agree', 'argument',
                    'platitude', 'feeling', 'flippant', 'criticism', 'implication', 'redditors', 'insinuation',
                    'perspective', 'characterization', 'ambivalence', 'criticisms', 'snark', 'respectfully', 'irony',
                    'thread', 'opinions', 'digress', 'topic', 'disagreeing', 'notion', 'response', 'vitriol', 'view',
                    'disdain', 'tone', 'cynicism', 'statement', 'disagreement', 'sentiments', 'points',
                    'agreeance', 'vehement', 'observation', 'remark', 'agreement', 'message', 'retort', 'diatribe',
                    'attack', 'dislike', 'viewpoints', 'counterargument', 'generalizing', 'understandable', 'reply',
                    'generalization', 'discussion', 'disagree', 'knee-jerk', 'post', 'counter-argument', 'misguided',
                    'comments', 'counterpoint', 'arguments', 'attitude', 'mindset','talking'): 'sentiment',
                   ('strategy', 'regression', 'bullish', 'predict', 'winner', 'result', 'counterfactual', 'conclude',
                    'estimations', 'observation', 'speculating', 'speculations', 'estimation', 'hypothetical',
                    'predictions', 'score', 'money', 'models', 'feeling', 'power', 'value', 'contention', 'projections',
                    'true', 'prognostication', 'speculation', 'thinking', 'time-frame', 'certainties', 'outlier',
                    'extrapolation', 'information', 'speculative', 'predicted', 'outcomes', 'outcome', 'hedging',
                    'speculate', 'anomaly', 'scale', 'predicts', 'improbability', 'scenario', 'forecasting',
                    'statistical', 'predicting', 'guesses', 'winning', 'fluke', 'toss', 'probabilities', 'analysis',
                    'wager', 'guessing', 'coincidence', 'flip', 'conclusion', 'forecasts', 'odds', 'data', 'results',
                    'event', 'improbably', 'predictive', 'theory', 'point', 'certainty', 'probability', 'bet', 'error',
                    'postulation', 'bias', 'timeframe', 'paradox', 'proved', 'conjecture', 'comeback', 'indisputable',
                    'assumption', 'observations', 'trends', 'flipping', 'evidence', 'trajectory', 'ability',
                    'optimistically', 'performance', 'standings', 'hypothesis', 'turnaround', 'mathematically',
                    'proves', 'upset', 'statement', 'forecast', 'ball', 'predication', 'wisdom', 'predicition',
                    'guess','next','will','prediction','future','tomorrow'): 'prediction',
                   ('consideration', 'account', 'metric', 'effect', 'function', 'indicator', 'measure', 'equation',
                    'risk', 'measurement', 'significant', 'correlates', 'proportional', 'variables', 'accounting',
                    'discrepancy', 'negligible', 'considerations', 'depend', 'disparity', 'likelihood', 'decrease',
                    'affect', 'degree', 'amount', 'variability', 'impact', 'factoring', 'difference', 'factors',
                    'matter', 'affects', 'overstated', 'factored', 'relative', 'astronomical', 'estimation',
                    'variances', 'irrespective', 'fluctuation', 'concern', 'decreases', 'variable', 'marginal',
                    'miniscule', 'increase', 'correlation', 'increases', 'greater', 'driver', 'component',
                    'considerable', 'question', 'relationship', 'minuscule', 'trade-off', 'issue',
                    'variance','influence'): 'factors'
                   }

sent_1 = "What is tomorrow Bitcoin price?"
sent_2 = "Future price of Bitcoin?"
sent_3 = "Current rate of Bitcoin?"
sent_4 = "Today price of Bitcoin?"
sent5 = 'what people are talking about bitcoin?'

data1 = [sent_1, sent_2, sent_3, sent_4, sent5]


def return_category(text):
    final = []

    data = text.split()
    for i in data:

        for m, na in main_categories.items():
            if i in m:
                print(na)
                final.append(na)
    try:
        return final[0]
    except IndexError:
        return 'This Feature is Not available this time'

###############################################################################################################


#second layer start



gh=['what will be the bitcoin price yesterday','what is tomorrow price','what will be the bitcoin price today','what will be the bitcoin price a day after tomorrow',"Hello 100 days ago what was time","Hello 103days ago what was time","Hello 1033         days ago what was time","Hello what will be the time 105 day after","Can you tell me time for 1066 days before","what is possibility for before 1034 days","Can you tell what will happen after 1023 days ?","10387 days","101     days"]
dict_tense={('ago','before','earlier','early'):'past',('after','tomorrow','after?'):'future',('today','present'):'today',('yesterday',):'yesterday',('day after tomorrow',):'day after tomorrow'}

pattern=r'yesterday|today|day after tomorrow|tomorrow'
pattern2=r'\d+(?=\s*days?)'
def calculate_past(days):
    return datetime.date.today()-timedelta(days=days)



def calculate_future(days):
    return datetime.date.today()+timedelta(days=days)


tense={'today':requests.get('https://www.coinbase.com/api/v2/prices/USD/spot?').json()['data'][0]['amount'],'yesterday':datetime.date.today()-timedelta(days=1),'tomorrow':datetime.date.today()+timedelta(days=1),'day after tomorrow':datetime.date.today()+timedelta(days=2),'past':calculate_past,'future':calculate_future}

def first_layer_checking(text):
    if re.findall(pattern,text):
        return re.findall(pattern,text)
    else:
        return 0




def second_layer(text):
    if isinstance(first_layer_checking(text),list):
        return tense[first_layer_checking(text)[0]]
    else:
        for i in text.split():
            for m,j in dict_tense.items():
                for k in m:
                    if i==k:
                        if re.findall(pattern2,text)!=None:
                            return (tense[j](int(re.findall(pattern2, text)[0])))





# ori='day after'
# nice='hello 10 day ago what'
# for i in re.findall(pattern,text):
#     print(int([j.strip() for j in i][0]))




#############
# Routing

# @app.route('/message', methods=['POST'])
# def reply():
#     if request.form['msg']=='price':
#         url = 'https://www.coinbase.com/api/v2/prices/USD/spot?'
#         for i in requests.get(url).json()['data']:
#             print(i['base'], i['currency'], i['amount'])
#             return jsonify({ 'text': ("{} Current price is {} ${}".format(i['base'], i['currency'], i['amount']))})
#
# print(return_category('what is bitcoin price today'))


@app.route('/message', methods=['POST'])

def reply():
    if 'today' in request.form['msg']:
        return jsonify({ 'text': tense['today']})
    elif 'yesterday' in request.form['msg']:
        return jsonify({'text': 'Yesterday Price of Bitcoin was 9000 \n graph is available here <a href="http://cryptroix.com/temp-plot.html" target="_blank">Link of Graph</a>'})

    elif 'invest'  in request.form['msg']:

        #  currently few responses are hard coded later we will do it using NLP/NLU by fetching Entities from sentence and understanding the meaning of sentence


        return jsonify({'text': 'Sentiment is 4.0 , And prediction for tomorrow is 19000 , we are 40% sure about prediction , If you want to invest'
                                'Basis on these data you can.'})


    else:
        first = second_layer(request.form['msg'])
        print("yes", first)
        print("no", return_category(request.form['msg']))

        if first == None:
            return jsonify({'text': "This Feature is Not available this time"})
        else:
            if return_category(request.form['msg']) == 'price':
                print('accha')
                print("hmm",return_category(request.form['msg']), first)
                print(accessing_database.detabase_result_fetch(return_category(request.form['msg']), str(first)))
                return jsonify({'text': (
                accessing_database.detabase_result_fetch(return_category(request.form['msg']), str(first)))})
            else:
                print("nhi")
                print("ohh",return_category(request.form['msg']))
                print(accessing_database.detabase_result_fetch(return_category(request.form['msg'])))
                return jsonify({'text': accessing_database.detabase_result_fetch(return_category(request.form['msg']))})






@app.route("/")
def index():

    return render_template("index.html")

# #############


#
# '''
# Init seq2seq model
#
#     1. Call main from execute.py
#     2. Create decode_line function that takes message as input
# '''
#
# #_________________________________________________________________
# # import sys
# # import os.path
# #
# # sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), os.path.pardir)))
# # import tensorflow as tf
# # import execute
# #
# # sess = tf.Session()
# # sess, model, enc_vocab, rev_dec_vocab = execute.init_session(sess, conf='seq2seq_serve.ini')
# #_________________________________________________________________
#
#
# start app
if (__name__ == "__main__"):
    app.run(port = 5004)



