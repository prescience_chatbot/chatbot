#understanding and classifying what user is asking

import requests
from datetime import timedelta
import datetime



main_categories = {('price', 'price?', 'amount', 'cost', 'demand', 'estimate', 'fare', 'fee', 'figure', 'output', 'pay',
                    'payment','return', 'score', 'wages', 'sell''price-point', 'priced',
                    'discounted', 'rate', 'difference', 'disparity', 'msrp', 'jump', 'nzd', 'differential', 'deal',
                    'msrp.', 'usd.', 'resale', 'tag', 'value', 'points', 'drop', 'sell', 'decrease', 'rrp',
                    'pricepoint', 'pricing', 'level', 'discount', 'margin','jumps', 'costs',
                    '80+', 'cut','prices', 'hike', 'change', 'mark-up', 'pricetag', 'drops','market',
                    'increase', 'premium', 'cap', 'increases','ago','before','earlier','early','current','ago','before','earlier','early','today','present','recent','just','latest','now','todays','few','current',"today's",'day after tomorrow'): 'price',

                   ('views','merits', 'argue', 'opionion','disagreeing',
                    'opnion', 'preference', 'dissagree', 'obviously', 'feelings', 'dislike',
                    'opinons', 'objectively', 'counterargument','feeling',
                    'bias', 'regard', 'arguements','stance', 'standpoints', 'one', 'oppinions', 'honesty',
                    'matter','reasons', 'merit','viewpoint', 'arguement',
                    'oppinion', 'again', 'reasoning', 'mate','criticisms', 'assessment',
                    'disagree', 'fact', 'opinon', 'statement', 'perspective', 'standpoint', 'preferences', 'disliking',
                    'pessimism', 'vehemence', 'paragraph','way', 'reaction',
                    'disagreeable','distaste', 'generalisation', 'debate', 'condescension',
                    'wholeheartedly', 'flippancy', 'comment', 'conversation', 'commenter','agree', 'argument',
                    'platitude','flippant', 'criticism', 'implication', 'redditors', 'insinuation',
                    'characterization', 'ambivalence','snark', 'respectfully', 'irony',
                    'thread', 'opinions', 'digress', 'topic','notion', 'response', 'vitriol', 'view',
                    'disdain', 'tone', 'cynicism','disagreement', 'sentiments',
                    'agreeance', 'vehement', 'observation', 'remark', 'agreement', 'message', 'retort', 'diatribe',
                    'attack','viewpoints','generalizing', 'understandable', 'reply',
                    'generalization', 'discussion','knee-jerk', 'post', 'counter-argument', 'misguided',
                    'comments', 'counterpoint', 'arguments', 'attitude', 'mindset', 'talking', 'sentiment',
                    'discussing', 'discuss', 'opinion', 'think', 'thoughts', 'thought', 'thinking','point of view','people','world',"people's"): 'sentiment',

                   ('strategy', 'regression', 'bullish', 'predict', 'winner', 'result', 'counterfactual', 'conclude',
                    'estimations','speculating', 'speculations', 'estimation', 'hypothetical',
                    'predictions','money', 'models','power', 'contention', 'projections',
                    'true', 'prognostication', 'speculation', 'time-frame', 'certainties', 'outlier',
                    'extrapolation', 'information', 'speculative', 'predicted', 'outcomes', 'outcome', 'hedging',
                    'speculate', 'anomaly', 'scale', 'predicts', 'improbability', 'scenario', 'forecasting',
                    'statistical', 'predicting', 'guesses', 'winning', 'fluke', 'toss', 'probabilities', 'analysis',
                    'wager', 'guessing', 'coincidence', 'flip', 'conclusion', 'forecasts', 'odds', 'data', 'results',
                    'event', 'improbably', 'predictive', 'theory','certainty', 'probability', 'bet', 'error',
                    'postulation','timeframe', 'paradox', 'proved', 'conjecture', 'comeback', 'indisputable',
                    'assumption', 'observations', 'trends', 'flipping', 'evidence', 'trajectory', 'ability',
                    'optimistically', 'performance', 'standings', 'hypothesis', 'turnaround', 'mathematically',
                    'proves', 'upset','forecast', 'ball', 'predication', 'wisdom', 'predicition',
                    'guess', 'next','prediction', 'future','day after tomorrow','after','after?','next','after','tomorrow','after?','next','forward'): 'prediction',

                   ('consideration', 'account', 'metric', 'effect', 'function', 'indicator', 'measure', 'equation',
                    'risk', 'measurement', 'significant', 'correlates', 'proportional', 'variables', 'accounting',
                    'discrepancy', 'negligible', 'considerations', 'depend','likelihood',
                    'affect', 'degree','variability', 'impact', 'factoring','factors',
                    'affects', 'overstated', 'factored', 'relative', 'astronomical',
                    'variances', 'irrespective', 'fluctuation', 'concern', 'decreases', 'variable', 'marginal',
                    'miniscule','correlation','greater', 'driver', 'component',
                    'considerable', 'question', 'relationship', 'minuscule', 'trade-off', 'issue',
                    'variance', 'influence','affecting','factors'): 'factors'
                   }
#will , #tomorrow, #tomorrow?



#current date for default use_case
#in_future it will be today_date
default_data='2018 03 13'
today=datetime.datetime.strptime(default_data,'%Y %m %d').date()

today+timedelta(days=1)

# calculating of past days
def calculate_past(days):
    return today-timedelta(days=days)






# calculating future days
def calculate_future(days):
    return today+timedelta(days=days)

month_dict={'jan':1,'feb':2,'mar':3,'apr':4,'may':5,'june':6,'july':7,'aug':8,'sep':9,'oct':10,'nov':11,'dec':12,'january':1,'february':2,'march':3,'april':4,'august':8,'september':9,'october':10,'november':11,'december':12,'last month':str(today-timedelta(days=27)).split('-')[1],'this month':str(today).split('-')[1]}

# print(month_dict)

months_list=['january','february','march','april','may','june','july','august','september','october','november','december','jan','feb','mar','apr','jun','aug','sep','oct','nov','dec','last month','current month','this month','previous month']
#question should be from this list vocabulary

question_vocabulary=set([k for i in main_categories for k in i] + ['bitcoin'] + ['january','february','march','april','may','june','july','august','september','october','november','december','jan','feb','mar','apr','jun','aug','sep','oct','nov','dec','last month','current month','this month','previous month'])
#out_of_scope questions

out_of_scope=['Sorry , This feature is not available this time :)','I am not google :/','I think i am bot not human hui hui ','Ohh so you are expecting too much from me haa','And i thought you will ask something related to Bitcoin','I need more training i think','I will tell my father to teach that :D',"I am afraid i can't answer this question",'Oh Wow I will learn this thing next time and will come back','I thought we were talking about Bitcoin right ? ']



#tense dict

dict_tense={('ago','before','earlier','early'):'past',('after','tomorrow','after?','next','forward'):'future',('today','present','recent','just','latest','now','todays','few','current',"today's"):'today',('yesterday','last','previous','earlier','went',"yesterday's"):'yesterday',('day after tomorrow',):'day after tomorrow'}

#action on appropriate tense

tense={'today':"Hi, today price is {}".format(requests.get('https://www.coinbase.com/api/v2/prices/USD/spot?').json()['data'][0]['amount']),'yesterday':today-timedelta(days=1),'tomorrow':today+timedelta(days=1),'day after tomorrow':today+timedelta(days=2),'past':calculate_past,'future':calculate_future,'last':today-timedelta(days=1)}
