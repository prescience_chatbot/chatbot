from flask import Flask, render_template, request
from flask import jsonify
import bot_reply
import accesing_big_quer
import datetime

app = Flask(__name__,static_url_path="/static")

default_data='2018 03 13'
today=datetime.datetime.strptime(default_data,'%Y %m %d').date()

query_past='SELECT {} FROM `solid-ruler-197805.prescience_production.model_data` WHERE Date = "{}" LIMIT 100'
query='SELECT {} FROM `solid-ruler-197805.prescience_production.prediction` WHERE Date = "{}" LIMIT 100'


def write_converation(text_):
    with open('conversation_log.txt','a') as f:
        f.write(text_)



@app.route('/message', methods=['POST'])

def reply():

    if request.form['msg'].lower() in ['hello','hi','morning','hey',"what's up?","what's up"]:
        write_converation("user_questiion {} \n\n bot reply : {} ".format(request.form['msg'].lower(),bot_reply.reply_from_bot(request.form['msg'])))

        return jsonify({ 'text': 'Hello Hi , How can i help you ? \n You can ask information related to Bitcoin , some use cases are : Today , Tomorrow , Yesterday bitcoin price , Influence Factors , Sentiments. '})


    elif all(word in request.form['msg'] for word in ['What', 'is', 'bitcoin?']):
        return jsonify({'text': 'Bitcoin is a cryptocurrency and worldwide payment '
                                'system. It is the first decentralized digital currency, as the system works without a central bank or single administrator.'})

    elif all(word in request.form['msg'] for word in ['who', 'invented']):
        return jsonify({
                           'text': 'Satoshi Nakamoto is the name used by the unknown person or people who designed bitcoin and created its original reference implementation. As part of the implementation, they also devised the first blockchain database'})

    elif all(word in request.form['msg'] for word in ['invest', 'bitcoin']):

        #  currently few responses are hard coded later we will do it using NLP/NLU by fetching Entities from sentence and understanding the meaning of sentence


        return jsonify({
                           'text': 'Sentiment is {} , And prediction for tomorrow is {} , we are {} sure about prediction , If you want to invest'
                                   'based on this data you can.'.format(str(accesing_big_quer.data_return(query_past.format('avg_sentiment','2018-03-13'))),accesing_big_quer.data_return(query.format('prediction_with_sentiment','2018-03-14')),'40%')})


    else:
        write_converation("user_questiion {} \n\n bot reply : {} ".format(request.form['msg'].lower(),bot_reply.reply_from_bot(request.form['msg'])))

        return jsonify({ 'text': bot_reply.reply_from_bot(request.form['msg'])})





















@app.route("/")
def index():

    return render_template("index.html")



# start app
if (__name__ == "__main__"):
    app.run(host='0.0.0.0',port=5000)
