import os
import numpy as np
import pandas as pd
# from selenium import webdriver
import plotly.plotly as pya
import datetime

# driver=webdriver.PhantomJS()
import pickle
import quandl
from datetime import datetime
import datetime as tt
import plotly.offline as py
import plotly.graph_objs as go
import plotly.figure_factory as ff
py.init_notebook_mode(connected=True)

today=datetime.today()
yesterday=today-tt.timedelta(days=1)
print(yesterday.date())


def get_quandl_data(quandl_id):
    '''Download and cache Quandl dataseries'''
    cache_path = '{}.pkl'.format(quandl_id).replace('/','-')
    try:
        f = open(cache_path, 'rb')
        df = pickle.load(f)
        print('Loaded {} from cache'.format(quandl_id))
        print(df)
    except (OSError, IOError) as e:
        print('Downloading {} from Quandl'.format(quandl_id))
        df = quandl.get(quandl_id, returns="pandas",authtoken='8YEnMD5jgnzdzz4sBixY',start_date='2014-01-07', end_date=str(yesterday.date()))
        df.to_pickle(cache_path)
        print('Cached {} at {}'.format(quandl_id, cache_path))
    return df

btc_usd_price_kraken = get_quandl_data('BCHARTS/KRAKENUSD')
btc_usd_price_kraken.head()
btc_trace = go.Scatter(x=btc_usd_price_kraken.index, y=btc_usd_price_kraken['Weighted Price'])
py.plot([btc_trace])
