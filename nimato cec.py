from halo import Halo

spinner = Halo(text='Loading', spinner='dots')
spinner.start()

import tensorflow
spinner.stop()