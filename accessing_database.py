import csv


def detabase_result_fetch(column_name,dates=0):
    with open('database.csv') as f:
        reader = csv.DictReader(f)
        rows = [row for row in reader]
    if column_name=='sentiment':
        for i in rows:
            if i['dates'] == '2018-03-14':
                return "sentiment is {} , so people are talking positive ".format(i['sentiment'])
    elif column_name=='factors':
        for i in rows:
            if i['dates'] == '2018-03-14':
                return i['factors']
    elif column_name=='prediction':
        for i in rows:
            if i['dates'] == '2018-03-14':
                return i['future_price']
    elif column_name=='price':
        for i in rows:
            if i['dates']==dates:
                return i['future_price']








