from selenium import webdriver
import requests
import re

sentence_pattern =r'<span style="color:.+?<\/span>'

pattern_code=r'background:(.+)?>(\w.+)?<\/span>'



driver=webdriver.PhantomJS()
driver.set_window_size(1120, 550)
url='http://nlp.stanford.edu:8080/ner/process'
driver.get(url)
data=driver.find_element_by_xpath('//*[@id="ContentBody"]/form/table/tbody/tr[4]/td/textarea').send_keys('Apple is looking at buying U.K. startup for $1 billion')
submit=driver.find_element_by_xpath('//*[@id="ContentBody"]/form/table/tbody/tr[5]/td/input[1]').click()
data_kj=re.findall(sentence_pattern,driver.page_source)


entities={}
for i in data_kj:
    final=re.findall(pattern_code,i)
    for j in final:
        if j[0] not in entities:
            entities[j[0]]=[j[1]]
        else:
            entities[j[0]].append(j[1])


print(entities.values())


