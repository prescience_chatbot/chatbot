import adal

authority_url = 'https://login.microsoftonline.com/testcompany.onmicrosoft.com'
context = adal.AuthenticationContext(
    authority_url,
    validate_authority=True,
    api_version=None
)

token = context.acquire_token_with_username_password(
    resource='https://analysis.windows.net/powerbi/api',
    username='john.doe@testcompany.onmicrosoft.com',
    password='secret',
    client_id='22a6fc5a-46fe-4f85-8168-281ece441742'
)

access_token = token['accessToken']