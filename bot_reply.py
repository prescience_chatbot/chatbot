#importing libraries
from halo import Halo

spinner = Halo(text='Loading', spinner='dots')
spinner.start()
print("Loading date and time ")
import datetime

from datetime import timedelta
print("Loaded")
spinner.stop()
spinner.start()
print("Loading Google api's ")
import google.api_core.exceptions
import requests
from entity_extraction_google import entities_text
print("Loaded")
spinner.stop()
spinner.start()
print("Loading word_embedding and vocabulary data ")


import words_embeddings_data
print("Loaded")
spinner.stop()

spinner.start()
print("Loading regex , big query  ")
import random
import re
import accesing_big_quer
import pricest
import checking
print("Loaded")
spinner.stop()

print("All libraries imported")




# query='SELECT {} FROM [solid-ruler-197805:prescience_production.prediction] WHERE DATE(Date) = "{}" LIMIT 100'

query='SELECT {} FROM `solid-ruler-197805.prescience_production.prediction` WHERE Date = "{}" LIMIT 100'

query_past='SELECT {} FROM `solid-ruler-197805.prescience_production.model_data` WHERE Date = "{}" LIMIT 100'

sentiment_query='SELECT {} FROM `solid-ruler-197805:prescience_production.model_data` WHERE Date = "{}" LIMIT 100'

print("all libraries imported")

pattern=r'yesterday|today|day after tomorrow|tomorrow|last|future|past'
pattern2=r'\d+(?=\s*days?)'



default_data='2018 03 13'
today=datetime.datetime.strptime(default_data,'%Y %m %d').date()


tables={'prediction':'prediction_with_sentiment','factors':'factors','price':'bitcoin_price','sentiment':'avg_sentiment'}

past_data=['sentiment','price','yesterday']

future_data=['prediction','factors','tomorrow']





#calculating of past days
# def calculate_past(days):
#     return today-timedelta(days=days)



#calculating future days
# def calculate_future(days):
#     return today+timedelta(days=days)


#checking if question is relevant or not
def validation_layer(text):

    try:
        data=set([j for i in entities_text(text) for j in i.split()])
        print("validation layer cleared")
        if data.issubset(words_embeddings_data.question_vocabulary):
            return data
        else:
            return False

    except:
        return 'something went wrong_ we are working on it'



def price_classification(text):

    data=classification(text)

    if data[0]=='price':
        print("price_classification layer")

        if re.search(pattern,text):
            # print("something",words_embeddings_data.tense[re.search(pattern,text).group()])
            return ['price',words_embeddings_data.tense[re.search(pattern,text).group()]]

        else:
            data_sub=[n for i in text.split() for m,n in words_embeddings_data.dict_tense.items() if i in m]
            print("query is related to past , future or today",data_sub)
            if data_sub:
                if data_sub[0] == 'past':
                    return ['price',words_embeddings_data.tense['past'](int(re.search(pattern2, text).group()))]
                elif data_sub[0] == 'future':
                    return ['price',words_embeddings_data.tense['future'](int(re.search(pattern2, text).group()))]

                elif data_sub[0] == 'today':
                    # print(words_embeddings_data.tense['today'])
                    return words_embeddings_data.tense['today']
                elif data_sub[0] == 'yesterday':
                    return ['price',words_embeddings_data.tense['yesterday']]

                elif data_sub[0] == 'tomorrow':
                    return ['price',words_embeddings_data.tense['tomorrow']]
            else:
                return 'something went wrong'
    elif data[0]=='sentiment':
        return ['sentiment',today]

    elif data[0]=='factors':
        return ['factors',today+timedelta(days=1)]

    elif data[0]=='prediction':
        return ['prediction',today+timedelta(days=1)]

    elif data=='something went wrong_ we are working on it':
        return 'something went wrong_ we are working on it'

    else:
        return random.choice(words_embeddings_data.out_of_scope)





















def classification(text):
    lower_data="".join([i.lower() for i in text])
    # print("lower_layer",lower_data)

    validation=validation_layer(lower_data)
    print("Classification layer")
    # print(validation)



    if validation=='something went wrong_ we are working on it':
        return 'something went wrong_ we are working on it'

    elif validation:
        category = [category for i in lower_data.split() for keywords, category in
                    words_embeddings_data.main_categories.items() if i in keywords]
        print(category)

        if category:
            print("user is asking {}".format(category))
            return category
        else:
            return random.choice(words_embeddings_data.out_of_scope)




    else:
        return random.choice(words_embeddings_data.out_of_scope)


validation_data=["what is today's bitcoin price",'what was bitcoin price yesterday','what is the prediction for tomorrow','which factors influencing bitcoin','what are people talking about bitcoin around the world','what will be the price tomorrow','what is the opinion of people on bitcoin','what factors affect the price of bitcoin','what people are thinking about bitcoin','what is today bitcoin price','what was yesterday bitcoin price','what will be bitcoin price tomorrow','what is bitcoin prediction','what is tomorrow prediction','what was recent bitcoin price','what is latest bitcoin price','what was bitcoin price 10 days before','what will be the bitcoin price after 10 days','what people are talking about bitcoin','which factors are influencing bitcoin','what is my shirt price','today is my birthday','who founded bitcoin','bitcoin is really expensive','people talking too much','what is future of machine learning']
bn=['can i hike with you']
# validation_fo=['what is my shirt price','today is my birthday','who founded bitcoin','bitcoin is really expensive','people talking too much','what is future of machine learning']
# bn=['can i hike with you']


# print(price_classification('which factors are influencing bitcoin'))
# # validation_again=["what is today's bitcoin price",'what was bitcoin price yesterday','what is the prediction for tomorrow','which factors influencing bitcoin','what are people talking about bitcoin around the world','what will be the price tomorrow','what is the opinion of people on bitcoin','what factors affect the price of bitcoin','what people are thinking about bitcoin']
# while True:
#     user_query = input('input > ')
#     if checking.checking_month_data(user_query):
#         print(checking.checking_month_data(user_query))
#
#     else:
#         check_len = user_query.split()
#         if len(check_len) <= 3:
#             if pricest.first_layer_check(user_query):
#                 print(pricest.first_layer_check(user_query))
#
#         else:
#             if len(price_classification(user_query)) == 2:
#                 data, table_name = price_classification(user_query)
#                 print(data,table_name)
#                 if data in past_data:
#                     try:
#                         if data=='sentiment':
#                             print(accesing_big_quer.data_return(query_past.format(tables[data],table_name)))
#                         elif data=='price':
#                             print("past_graph",accesing_big_quer.data_return(query_past.format(tables[data],table_name)))
#
#                     except google.api_core.exceptions.BadRequest:
#                         print(table_name)
#
#                 elif data in future_data:
#                     print(data, table_name)
#                     if data in tables:
#                         if data=='factors':
#                             print(accesing_big_quer.data_return(query.format(tables[data], table_name)))
#
#                         elif data=='prediction':
#                             print("graph_future",accesing_big_quer.data_return(query.format(tables[data], table_name)))
#
#
#
#                         # print('------------------------------\n\n\n\n\n\n\n')
#
#             else:
#                 print("Feature is not available yet")

    # check_len=user_query.split()
    # if len(check_len)<=3:
    #     if pricest.first_layer_check(user_query):
    #         print(pricest.first_layer_check(user_query))
    #
    #
    # else:
    #     if len(price_classification(user_query)) == 2:
    #         data, table_name = price_classification(user_query)
    #         print(data)
    #
    #         print("thisss", data, table_name)
    #         if data in tables:
    #             print(accesing_big_quer.data_return(query.format(tables[data], table_name)))
    #             print("waha se result")
    #             print('------------------------------\n\n\n\n\n\n\n')
    #
    #     else:
    #         print("Feature is not available yet")

def reply_from_bot(user_query):
    if checking.checking_month_data(user_query):
        return str(checking.checking_month_data(user_query))

    else:
        check_len = user_query.split()
        if len(check_len) <= 3:
            if pricest.first_layer_check(user_query):
                return str(pricest.first_layer_check(user_query))

        else:
            if len(price_classification(user_query)) == 2:
                data, table_name = price_classification(user_query)
                print("final_result layer for google big query")
                print(data,table_name)
                if data in past_data:
                    try:
                        if data=='sentiment':
                            #define threshold value for giving output like sentiment is positive or negative
                            return str(accesing_big_quer.data_return(query_past.format(tables[data],table_name)))
                        elif data=='price':
                            return str("Past price of Bitcoin is  {}  Graph is Available here {}".format(accesing_big_quer.data_return(query_past.format(tables[data],table_name)),'<a href="https://bit.ly/2FlK1oQ" target="_blank">Link of Graph</a>'))

                    except google.api_core.exceptions.BadRequest:
                        return str(['price',table_name])

                elif data in future_data:
                    print("final_result layer for google big query")

                    print(data, table_name)
                    if data in tables:
                        if data=='factors':
                            return str(accesing_big_quer.data_return(query.format(tables[data], table_name)))

                        elif data=='prediction':
                            return str("Prediction for Bitcoin is {}  Graph is Available here {}".format(accesing_big_quer.data_return(query.format(tables[data], table_name)),'<a href="https://bit.ly/2r0lYr6" target="_blank">Link of Graph</a>'))



                        # print('------------------------------\n\n\n\n\n\n\n')

            else:
                return price_classification(user_query)


















# validation_fo=['what is my shirt price','today is my birthday','who founded bitcoin','bitcoin is really expensive','people talking too much','what is future of machine learning']













