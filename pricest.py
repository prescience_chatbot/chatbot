# with open('text_file','r') as f:
#     print([line.strip() for line in f if line!='\n'])


shirt_question=['bitcoin Today', 'price today', 'sentiment today', 'factors today', 'people thinking', 'prediction', 'prediction for tomorrow', 'tomorrow prediction', 'future price', 'tomorrow price', 'yesterday price', 'last price', 'previous price', 'recent price', 'current price', 'latest price', 'now price', 'price', 'sentiment', 'factors', 'prediction', 'price of tomorrow', 'price for tomorrow', 'current']
from datetime import timedelta
import datetime
import requests
import sys
import unicodedata
from accesing_big_quer import data_return

query_past='SELECT {} FROM `solid-ruler-197805.prescience_production.model_data` WHERE Date = "{}" LIMIT 100'

default_data='2018 03 13'
today=datetime.datetime.strptime(default_data,'%Y %m %d').date()

query='SELECT {} FROM `solid-ruler-197805.prescience_production.prediction` WHERE Date = "{}" LIMIT 100'

punctuation=dict.fromkeys([i for i in range(sys.maxunicode) if unicodedata.category(chr(i)).startswith('P')])

def remove_pun(sentence):
    return sentence.translate(punctuation)


NOISE_WORDS=['a', 'about', 'above', 'after', 'again', 'against', 'all', 'am', 'an', 'and', 'any', 'are', "aren't", 'as', 'at', 'be', 'because', 'been', 'before', 'being', 'below', 'between', 'both', 'but', 'by', "can't", 'cannot', 'could', "couldn't", 'did', "didn't", 'do', 'does', "doesn't", 'doing', "don't", 'down', 'during', 'each', 'few', 'for', 'from', 'further', 'had', "hadn't", 'has', "hasn't", 'have', "haven't", 'having', 'he', "he'd", "he'll", "he's", 'her', 'here', "here's", 'hers', 'herself', 'him', 'himself', 'his', 'how', "how's", 'i', "i'd", "i'll", "i'm", "i've", 'if', 'in', 'into', 'is', "isn't", 'it', "it's", 'its', 'itself', "let's", 'me', 'more', 'most', "mustn't", 'my', 'myself', 'no', 'nor', 'not', 'of', 'off', 'on', 'once', 'only', 'or', 'other', 'ought', 'our', 'ours', 'ourselves', 'out', 'over', 'own', 'same', "shan't", 'she', "she'd", "she'll", "she's", 'should', "shouldn't", 'so', 'some', 'such', 'than', 'that', "that's", 'the', 'their', 'theirs', 'them', 'themselves', 'then', 'there', "there's", 'these', 'they', "they'd", "they'll", "they're", "they've", 'this', 'those', 'through', 'to', 'too', 'under', 'until', 'up', 'very', 'was', "wasn't", 'we', "we'd", "we'll", "we're", "we've", 'were', "weren't", 'what', "what's", 'when', "when's", 'where', "where's", 'which', 'while', 'who', "who's", 'whom', 'why', "why's", 'with', "won't", 'would', "wouldn't", 'you', "you'd", "you'll", "you're", "you've", 'your', 'yours', 'yourself', 'yourselves']



def noise_remove(text):
    text=text.split()
    return " ".join([i for i in text if i not in NOISE_WORDS])


dun=['factors today',  'factors', 'prediction',]




action_question={('bitcoin','today','price','now','latest','current','of','for','the','price', 'price?', 'amount', 'cost', 'demand', 'estimate', 'fare', 'fee', 'figure', 'output', 'pay',
                    'payment','return', 'score', 'wages', 'sell''price-point', 'priced',
                    'discounted', 'rate', 'difference', 'disparity', 'msrp', 'jump', 'nzd', 'differential', 'deal',
                    'msrp.', 'usd.', 'resale', 'tag', 'value', 'points', 'drop', 'sell', 'decrease', 'rrp',
                    'pricepoint', 'pricing', 'level', 'discount', 'margin','jumps', 'costs',
                    '80+', 'cut','prices', 'hike', 'change', 'mark-up', 'pricetag', 'drops','market',
                    'increase', 'premium', 'cap', 'increases','ago','before','earlier','early','is'):requests.get('https://www.coinbase.com/api/v2/prices/USD/spot?').json()['data'][0]['amount'],







                 ('is','sentiment','today','now','latest','current','thinking','people','of','for','the','views','merits', 'argue', 'opionion','disagreeing',
                    'opnion', 'preference', 'dissagree', 'obviously', 'feelings', 'dislike',
                    'opinons', 'objectively', 'counterargument','feeling',
                    'bias', 'regard', 'arguements','stance', 'standpoints', 'one', 'oppinions', 'honesty',
                    'matter','reasons', 'merit','viewpoint', 'arguement',
                    'oppinion', 'again', 'reasoning', 'mate','criticisms', 'assessment',
                    'disagree', 'fact', 'opinon', 'statement', 'perspective', 'standpoint', 'preferences', 'disliking',
                    'pessimism', 'vehemence', 'paragraph','way', 'reaction',
                    'disagreeable','distaste', 'generalisation', 'debate', 'condescension',
                    'wholeheartedly', 'flippancy', 'comment', 'conversation', 'commenter','agree', 'argument',
                    'platitude','flippant', 'criticism', 'implication', 'redditors', 'insinuation',
                    'characterization', 'ambivalence','snark', 'respectfully', 'irony',
                    'thread', 'opinions', 'digress', 'topic','notion', 'response', 'vitriol', 'view',
                    'disdain', 'tone', 'cynicism','disagreement', 'sentiments',
                    'agreeance', 'vehement', 'observation', 'remark', 'agreement', 'message', 'retort', 'diatribe',
                    'attack','viewpoints','generalizing', 'understandable', 'reply',
                    'generalization', 'discussion','knee-jerk', 'post', 'counter-argument', 'misguided',
                    'comments', 'counterpoint', 'arguments', 'attitude', 'mindset', 'talking',
                    'discussing', 'discuss', 'opinion', 'think', 'thoughts', 'thought', 'thinking','point of view','people','world'):data_return(query_past.format('avg_sentiment',today)),






                 ('tomorrow','price','prediction','future','to','for','the','of','strategy', 'regression', 'bullish', 'predict', 'winner', 'result', 'counterfactual', 'conclude',
                    'estimations','speculating', 'speculations', 'estimation', 'hypothetical',
                    'predictions','money', 'models','power', 'contention', 'projections',
                    'true', 'prognostication', 'speculation', 'time-frame', 'certainties', 'outlier',
                    'extrapolation', 'information', 'speculative', 'predicted', 'outcomes', 'outcome', 'hedging',
                    'speculate', 'anomaly', 'scale', 'predicts', 'improbability', 'scenario', 'forecasting',
                    'statistical', 'predicting', 'guesses', 'winning', 'fluke', 'toss', 'probabilities', 'analysis',
                    'wager', 'guessing', 'coincidence', 'flip', 'conclusion', 'forecasts', 'odds', 'data', 'results',
                    'event', 'improbably', 'predictive', 'theory','certainty', 'probability', 'bet', 'error',
                    'postulation','timeframe', 'paradox', 'proved', 'conjecture', 'comeback', 'indisputable',
                    'assumption', 'observations', 'trends', 'flipping', 'evidence', 'trajectory', 'ability',
                    'optimistically', 'performance', 'standings', 'hypothesis', 'turnaround', 'mathematically',
                    'proves', 'upset','forecast', 'ball', 'predication', 'wisdom', 'predicition',
                    'guess', 'next','prediction', 'future','day after tomorrow','after','after?','next','value'):"Value of Prediction data is {}  Graph is Available here {}".format(data_return(query.format('prediction_with_sentiment',today+timedelta(days=1))),'<a href="https://bit.ly/2r0lYr6" target="_blank">Link of Graph</a>'),





                 ('yesterday','price','previous','last','of','for','the','to'):"Value of Past data is {}  Graph is Available here {}".format(data_return(query_past.format('bitcoin_price',today-timedelta(days=1))),'<a href="https://bit.ly/2FlK1oQ" target="_blank">Link of Graph</a>'),



                 ('what','is','factors','influencing','consideration', 'account', 'metric', 'effect', 'function', 'indicator', 'measure', 'equation',
                    'risk', 'measurement', 'significant', 'correlates', 'proportional', 'variables', 'accounting',
                    'discrepancy', 'negligible', 'considerations', 'depend','likelihood',
                    'affect', 'degree','variability', 'impact', 'factoring','factors',
                    'affects', 'overstated', 'factored', 'relative', 'astronomical',
                    'variances', 'irrespective', 'fluctuation', 'concern', 'decreases', 'variable', 'marginal','today'
                    'miniscule','correlation','greater', 'driver', 'component',
                    'considerable', 'question', 'relationship', 'minuscule', 'trade-off', 'issue',
                    'variance', 'influence','affecting','factors'):data_return(query.format('factors',today+timedelta(days=1)))}



quesrt=['bitcoin Today', 'price today', 'sentiment today', 'factors today', 'people thinking', 'prediction', 'prediction for tomorrow', 'tomorrow prediction', 'future price', 'tomorrow price', 'yesterday price', 'last price', 'previous price', 'recent price', 'current price', 'latest price', 'now price', 'price', 'sentiment', 'factors', 'prediction', 'price of tomorrow', 'price for tomorrow', 'current', 'what is bitcoin today', 'highest value of bitcoin', 'what is bitcoin today']


def first_layer_check(text_):
    data=remove_pun(text_)
    print("firs_layer_checking")

    data=noise_remove(data).split()
    data=[i.lower() for i in data]

    for i,j in action_question.items():

        if set(data).issubset(set(i)):
            return j
    else:
        return False



